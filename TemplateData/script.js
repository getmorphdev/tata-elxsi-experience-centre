$(document).ready(function () {
    $('.button').click(function(){
        $('.overlay').fadeOut('fast');
        $('#bgvid').trigger('play');
        $('#bgvid').on('ended',function(){
            $('.webgl-content').css('opacity','1');
            $(this).fadeOut('slow');
            unityInstance.SendMessage('GameController', 'OnWebVideoEnd');
        });
    });
});
